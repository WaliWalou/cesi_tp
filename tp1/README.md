# TP1 : Configuration système

## I. Création et configuration
**1.** 	
```
sudo useradd toto -m -s /bin/bash -u 2000
sudo passwd toto
```
Changing password for user toto.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
	
**2.**	```sudo groupadd admins```

**3.**	
``` 
sudo visudo
cat /etc/sudoers
```

##Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
%admins ALL=(ALL)      ALL

**4.**	
```
sudo usermod -aG admins toto
```

**SSH**
```
ssh-keygen -t rsa -b 4096
cat C:\Users\Jayson/.ssh/id_rsa.pub.
```
**Copier la clé publique**
```
sudo vim /home/jlebras/.ssh/authorized_keys
```
**Puis coller la clé SSH et enregistrer**
```
chmod 600 /home/jlebras/.ssh/authorized_keys
chmod 700 /home/jlebras/.ssh
```

## II.	
**Nom de domaine**
```
vim /etc/hostname
sudo hostname linux1
```

**Serveur DNS**
```
vim /etc/resolv.conf
```

## III.	
**Partitionnement**
```
sudo pvcreate /dev/sdb
```
Physical volume "/dev/sdb" successfully created.
```
sudo pvcreate /dev/sdc
```
Physical volume "/dev/sdc" successfully created.
```
sudo vgcreate data /dev/sdb /dev/sdc
```
Volume group "data" successfully created
```
sudo vgs
```
VG         #PV     #LV     #SN     Attr       VSize   VFree
centos       1       2       0     wz--n-     <7.00g    0
data         2       0       0     wz--n-      5.99g 5.99g
```
sudo lvcreate -L 2G data -n part1
```
Logical volume "part1" created.
```
sudo lvcreate -L 2G data -n part2
```
Logical volume "part2" created.
```
sudo lvcreate -l 100%FREE data -n part3
```
Logical volume "part3" created.
```
sudo lvs
```
LV    VG     Attr       LSize   Pool 
  	root  centos -wi-ao----  <6.20g
 	swap  centos -wi-ao---- 820.00m
 	part1 data   -wi-a-----   2.00g
  	part2 data   -wi-a-----   2.00g
 	part3 data   -wi-a-----   1.99g
```
sudo mkfs -t ext4 /dev/data/part1
```
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
…
**Pareil pour part2 et part3**
```
sudo vim /etc/fstab
sudo cat /etc/fstab
```
/dev/data/part1 /mnt/part1                      ext4    defaults        0 0
/dev/data/part2 /mnt/part2                      ext4    defaults        0 0
/dev/data/part3 /mnt/part3                      ext4    defaults        0 0
```
sudo mkdir part1
sudo mkdir part2
sudo mkdir part3
sudo mount -av
```
/                        : ignored
/boot                    : already mounted
swap                     : ignored
/mnt/part1               : successfully mounted

## IV.
**Interaction avec un service existant**
```
systemctl is-active firewalld
```
active
```
systemctl is-enabled firewalld
```
enabled

**Création de service**
```
sudo vim /etc/systemd/system/web.service
sudo firewall-cmd --add-port=8888/tcp --permanent
sudo firewall-cmd --reload
sudo systemctl start web
sudo systemctl enable web
```
**Test avec :**
```
curl 10.88.88.10:8888
```
**Réponse :**
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>

**Modification de l’unité**
```
sudo adduser web
passwd web
cd /srv
mkdir web
sudo chown web web
sudo vim test.html
sudo chown web test.html
sudo vim /etc/systemd/system/web.service
```
[Unit]
Description=Very simple web service
[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
User=web
WorkingDirectory=/srv/web
[Install]
WantedBy=multi-user.target

**Test :**
```
curl localhost:8888
```

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="test.html">test.html</a>
</ul>
<hr>
</body>
</html>


		
