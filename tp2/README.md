# TP2 : Serveur Web

## I.	Base de données

**Sur db.tp2.cesi :**
```
sudo yum install mariadb-server
sudo systemctl start mariadb
sudo systemctl status mariadb
```
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-01-06 11:55:00 CET; 4s ago
**Pour lancer l’installation de la DB**
sudo mysql_secure_installation
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.
```
sudo systemctl restart mariadb
sudo mysql -u root -p
```
Welcome to the MariaDB monitor…
MariaDB [(none)]>
```
CREATE DATABASE dbWEB;
CREATE USER 'web'@'localhost' IDENTIFIED BY 'azer';
GRANT ALL ON dbWEB.* TO 'web'@'localhost' IDENTIFIED BY 'azer' WITH GRANT OPTION;
CREATE USER 'web'@'%' IDENTIFIED BY 'azer';
GRANT ALL PRIVILEGES ON dbWEB.* TO 'web'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EXIT;
```
```
sudo systemctl restart mariadb
```
**Verification :**
```
mysql -u web -p -h 10.99.99.12 -P 3306
SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| dbWEB              |
+--------------------+
```

## II.	Serveur Web

**Sur web.tp2.cesi :**
```
sudo yum install httpd
sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
sudo yum install -y yum-utils
sudo yum remove -y php
sudo yum-config-manager --enable remi-php56
sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
cd /tmp && wget http://wordpress.org/latest.tar.gz
sudo tar -xvzf latest.tar.gz -C /var/www/html
sudo chown -R apache /var/www/html/wordpress
```
**Puis on se connecte sur http://10.99.99.11 et on configure WordPress**

**Verification :**
```
curl 10.99.99.11
(fin du retour)

dow.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
</script>
</body>
</html>
```

## III.	Reverse Proxy

**Sur rp.tp2.cesi**
```
sudo yum install epel-release
sudo yum update
sudo yum install nginx
sudo systemctl start nginx && systemctl enable nginx
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd –reload
```
**Il suffit ensuite de modifier le fichier de conf de nginx**
```
sudo vim /etc/nginx/nginx.conf
```
**Verification :**
```
curl 10.99.99.13
(fin du retour)

ndow.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
        </script>

</body>
</html>
```